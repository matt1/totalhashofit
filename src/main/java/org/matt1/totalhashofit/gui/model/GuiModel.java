package org.matt1.totalhashofit.gui.model;


import java.util.HashMap;
import java.util.Map;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import org.matt1.totalhashofit.hash.functions.HashFunction;

/**
 * Model that represents the current GUI state
 * 
 * @author matt
 *
 */
public class GuiModel {

	public BooleanProperty md2 = new SimpleBooleanProperty();
	public BooleanProperty md4 = new SimpleBooleanProperty();
	public BooleanProperty md5 = new SimpleBooleanProperty();
	
	public BooleanProperty sha1 = new SimpleBooleanProperty();
	public BooleanProperty sha224 = new SimpleBooleanProperty();
	public BooleanProperty sha256 = new SimpleBooleanProperty();
	public BooleanProperty sha384 = new SimpleBooleanProperty();
	public BooleanProperty sha512 = new SimpleBooleanProperty();
	
	public BooleanProperty ripe128 = new SimpleBooleanProperty();
	public BooleanProperty ripe160 = new SimpleBooleanProperty();
	public BooleanProperty ripe256 = new SimpleBooleanProperty();
	public BooleanProperty ripe320 = new SimpleBooleanProperty();
	
	public BooleanProperty whirlpool = new SimpleBooleanProperty();
	public BooleanProperty tiger = new SimpleBooleanProperty();
	public BooleanProperty gost = new SimpleBooleanProperty();

	public StringProperty text = new SimpleStringProperty();
	
	public Map<HashFunction, BooleanProperty> hashes = new HashMap<HashFunction, BooleanProperty>();
	
	/** Selection state of child menus */
	public enum Selection {
		All,
		Some,
		None
	}
	
	public GuiModel() {
		hashes.put(HashFunction.MD2, md2);
		hashes.put(HashFunction.MD4, md4);
		hashes.put(HashFunction.MD5, md5);
		
		hashes.put(HashFunction.SHA1, sha1);
		hashes.put(HashFunction.SHA224, sha224);
		hashes.put(HashFunction.SHA256, sha256);
		hashes.put(HashFunction.SHA384, sha384);
		hashes.put(HashFunction.SHA512, sha512);
		
		hashes.put(HashFunction.RIPE128, ripe128);
		hashes.put(HashFunction.RIPE160, ripe160);
		hashes.put(HashFunction.RIPE256, ripe256);
		hashes.put(HashFunction.RIPE320, ripe320);
		
		hashes.put(HashFunction.WHIRLPOOL, whirlpool);
		hashes.put(HashFunction.GOST, gost);
		hashes.put(HashFunction.TIGER192_3, tiger);		
	}
		
	/**
	 * Gets selection state of the MD family menu
	 * 
	 * @return
	 */
	public Selection getMDSelection() {
		Selection result;
		
		if (md2.get() && md4.get() && md5.get()) {
			result = Selection.All;
		} else if (md2.get() || md4.get() || md5.get()) {
			result = Selection.Some;
		} else {
			result = Selection.None;
		}
		return result;
	}
	
	/**
	 * Gets selection state of the SHA family menu
	 * 
	 * @return
	 */
	public Selection getSHASelection() {
		Selection result;
		
		if (sha1.get() && sha224.get() && sha256.get() && sha384.get() && sha512.get()) {
			result = Selection.All;
		} else if (sha1.get() || sha224.get() || sha256.get() || sha384.get() || sha512.get()) {
			result = Selection.Some;
		} else {
			result = Selection.None;
		}
		return result;
	}
	
	/**
	 * Gets selection state of the RIPE family menu
	 * 
	 * @return
	 */
	public Selection getRIPESelection() {
		Selection result;
		
		if (ripe128.get() && ripe160.get() && ripe256.get() && ripe320.get()) {
			result = Selection.All;
		} else if (ripe128.get() || ripe160.get() || ripe256.get() || ripe320.get()) {
			result = Selection.Some;
		} else {
			result = Selection.None;
		}
		return result;
	}
	
	/**
	 * Gets selection state of the RIPE family menu
	 * 
	 * @return
	 */
	public Selection getMiscSelection() {
		Selection result;
		
		if (tiger.get() && gost.get() && whirlpool.get()) {
			result = Selection.All;
		} else if (tiger.get() || gost.get() || whirlpool.get()) {
			result = Selection.Some;
		} else {
			result = Selection.None;
		}
		return result;
	}
	
}





