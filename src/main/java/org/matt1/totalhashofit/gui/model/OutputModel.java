package org.matt1.totalhashofit.gui.model;

import org.matt1.totalhashofit.hash.functions.HashFunction;

/**
 * Model to contain the output of a hashing.  Used to make working with the results in the GUI
 * easier.
 * 
 * @author matt
 *
 */
public class OutputModel {

	private String filename;
	private String hash;
	private HashFunction hashFunction;

	public OutputModel(String hash, HashFunction function) {
		this.filename = "";
		this.hash = hash;
		this.hashFunction = function;
	}
	
	public OutputModel(String filename, String hash, HashFunction function) {
		this.filename = filename;
		this.hash = hash;
		this.hashFunction = function;
	}
	
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	public HashFunction getHashFunction() {
		return hashFunction;
	}
	public void setHashFunction(HashFunction hashFunction) {
		this.hashFunction = hashFunction;
	}
	
	
	
}
