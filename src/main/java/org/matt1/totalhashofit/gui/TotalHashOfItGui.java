package org.matt1.totalhashofit.gui;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.logging.Logger;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import org.apache.commons.io.IOUtils;
import org.controlsfx.dialog.Dialogs;
import org.matt1.totalhashofit.gui.model.GuiModel;
import org.matt1.totalhashofit.gui.model.GuiModel.Selection;
import org.matt1.totalhashofit.gui.model.OutputModel;
import org.matt1.totalhashofit.hash.HashFunctionFactory;
import org.matt1.totalhashofit.hash.Hasher;
import org.matt1.totalhashofit.hash.HasherException;




/**
 * GUI for total hash of it.
 * 
 * @author matt
 *
 */
public class TotalHashOfItGui implements Initializable {
	
	/** Default logger */
	private static Logger log  = Logger.getLogger(TotalHashOfItGui.class.getName());
	
	/** Data model */
	GuiModel model = new GuiModel();
	
	/** Bindings for controls */
	@FXML Parent root;
	@FXML private MenuItem loadFile;
	@FXML private MenuItem reset;
	@FXML private MenuItem close;
	@FXML private MenuItem about;
	
	@FXML private MenuItem debug;
	
	@FXML private MenuItem selectAll;
	@FXML private MenuItem selectNone;
	
	@FXML private MenuButton md;
	@FXML private MenuButton sha;
	@FXML private MenuButton ripe;
	@FXML private MenuButton misc;
	
	@FXML private CheckMenuItem md2;
	@FXML private CheckMenuItem md4;
	@FXML private CheckMenuItem md5;
	
	@FXML private CheckMenuItem sha1;
	@FXML private CheckMenuItem sha224;
	@FXML private CheckMenuItem sha256;
	@FXML private CheckMenuItem sha384;
	@FXML private CheckMenuItem sha512;

	@FXML private CheckMenuItem ripe128;
	@FXML private CheckMenuItem ripe160;
	@FXML private CheckMenuItem ripe256;
	@FXML private CheckMenuItem ripe320;
	
	@FXML private CheckMenuItem tiger;
	@FXML private CheckMenuItem gost;
	@FXML private CheckMenuItem whirlpool;

	@FXML private AnchorPane textInput;
	@FXML private AnchorPane fileInput;
	@FXML private TextArea text;
	
	@FXML private TabPane outputTabs;
	@FXML private Tab outputTable;
	@FXML private Tab outputCsv;
	@FXML private TextArea hashOutput;
	@FXML private TableView<OutputModel> hashTable;
	@FXML private TableColumn<OutputModel, String> fileColumn;
	@FXML private TableColumn<OutputModel, String> functionColumn;
	@FXML private TableColumn<OutputModel, String> hashColumn;
	
	@FXML private Label filename;
	@FXML private Label size;
	@FXML private Label path;
	
	/** If we are selecting all, we need to wait to stop spwaning loads of tasks before 
	 * we've updated everything!
	 */
	private Boolean waitForUpdate = false;
	
	/** Keep track of what we're hashing right now */
	private Boolean hashingText = true;
	
	/** Files we're hashing */
	private List<File> filesToHash = null;
	
	/** All menus for easier select all/none */
	private List<CheckMenuItem> allMenu = new ArrayList<CheckMenuItem>(30);
	
	/** Image for full selection */
	private Image fullSelection = new Image(getClass().getResourceAsStream("full.png"));
	
	/** Image for full selection */
	private Image noSelection = new Image(getClass().getResourceAsStream("none.png"));
	
	/** Image for full selection */
	private Image someSelection = new Image(getClass().getResourceAsStream("half.png"));
	
	/** Stage */
	private Stage stage;
	
	/**
	 * Called by JavaFX when initialisation of this controller is complete
	 */
	@Override
	public void initialize(URL fxmlFileLocation, ResourceBundle resource) {
		
		setupBinding();	
		setupListeners();
		updateSelectionIcons(); 
		
		// Default to one or two hashes to get started
		md5.setSelected(true);
		sha256.setSelected(true);
				
	}
		
	/**
	 * Set the stage and initialise Drag & drop
	 */
	public void setStage(Stage stage) {
		this.stage = stage;
		this.setupDragDrop(stage.getScene());
	}
	
	/** 
	 * Updates the selection icons for the sub menus - passes in lambda references to meet
	 * the java.util.function Supplier and Consumer signatures (get() and accept());
	 * 
	 */
	private void updateSelectionIcons() {
				
		setIcons(() -> {return model.getMDSelection();}, 
				(ImageView imageView) -> {md.setGraphic(imageView);});
		
		setIcons(() -> {return model.getSHASelection();}, 
				(ImageView imageView) -> {sha.setGraphic(imageView);});
		
		setIcons(() -> {return model.getRIPESelection();}, 
				(ImageView imageView) -> {ripe.setGraphic(imageView);});
		
		setIcons(() -> {return model.getMiscSelection();}, 
				(ImageView imageView) -> {misc.setGraphic(imageView);});
	
	}
	
	/**
	 * Set the icons for the given menu - uses java.util.function Supplier and Consumer 
	 * signatures to avoid repetition of same code for each menu
	 * 
	 * @param getSelection
	 * @param setGraphic
	 */
	private void setIcons(Supplier<Selection> getSelection, Consumer<ImageView> setGraphic) {
		
		// We need to create a new image view each time as otherwise a single image view 
		// can only "be in one place at a time" in JavaFX, so we couldn't reuse the same 
		// "full" image view on each menu if they are all full
		if (getSelection.get() == Selection.None) {
			setGraphic.accept(new ImageView(noSelection));
		} else if (getSelection.get() == Selection.Some) {
			setGraphic.accept(new ImageView(someSelection));
		} else {
			setGraphic.accept(new ImageView(fullSelection));
		}	
	}
	
	/**
	 * Setup drag & drop support
	 * 
	 * @param scene
	 */
	private void setupDragDrop(Scene scene) {
				
		scene.setOnDragOver(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                if (db.hasFiles() || db.hasString() || db.hasHtml() || db.hasRtf() || db.hasUrl()) {
                    event.acceptTransferModes(TransferMode.ANY);                 	
                } else {
                    event.consume();
                }
            }
        });
        
        // Dropping over surface
        scene.setOnDragDropped(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                boolean success = false;
                if (db.hasFiles()) {
                    success = true;                    
                    filesToHash = db.getFiles();
                    hashFile();
                    
                } else if (db.hasString()) {
                	text.setText(db.getString());
                } else if (db.hasHtml()) {
                	text.setText(db.getHtml());
                } else if (db.hasRtf()) {
                	text.setText(db.getRtf());
                } else if (db.hasUrl()) {
                	text.setText(db.getUrl());
                }
                event.setDropCompleted(success);
                event.consume();
            }
        });
        
	}	
	
	/**
	 * Setup binding to the model
	 */
	private void setupBinding() {
        model.md2.bind(md2.selectedProperty());
        model.md4.bind(md4.selectedProperty());
        model.md5.bind(md5.selectedProperty());
        
        model.sha1.bind(sha1.selectedProperty());
        model.sha224.bind(sha224.selectedProperty());
        model.sha256.bind(sha256.selectedProperty());
        model.sha384.bind(sha384.selectedProperty());
        model.sha512.bind(sha512.selectedProperty());
        
        model.ripe128.bind(ripe128.selectedProperty());
        model.ripe160.bind(ripe160.selectedProperty());
        model.ripe256.bind(ripe256.selectedProperty());
        model.ripe320.bind(ripe320.selectedProperty());
        
        model.gost.bind(gost.selectedProperty());
        model.tiger.bind(tiger.selectedProperty());
        model.whirlpool.bind(whirlpool.selectedProperty());
        
        model.text.bind(text.textProperty());
        
        allMenu.add(md2);
        allMenu.add(md4);
        allMenu.add(md5);
        
        allMenu.add(sha1);
        allMenu.add(sha224);
        allMenu.add(sha256);
        allMenu.add(sha384);
        allMenu.add(sha512);
        
        allMenu.add(ripe128);
        allMenu.add(ripe160);
        allMenu.add(ripe256);
        allMenu.add(ripe320);
        
        allMenu.add(tiger);
        allMenu.add(gost);
        allMenu.add(whirlpool);

	}
	
	/**
	 * Refresh the hash
	 */
	private void rehash() {
		if (hashingText) {
			hashText(text.textProperty().get());	
		} else {
			hashFile();
		}
		
	}
	
	public void setupListeners() {
        // Setup listeners for function menus
		for (CheckMenuItem item : allMenu) {
			item.selectedProperty().addListener((b) -> {
				updateSelectionIcons();
				rehash();
			});
		}
		
		
		// Text change listener
		text.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable,
					String oldValue, String newValue) {				
				hashText(newValue);
			}
			
		});
	}

	/**
	 * Update the hashes of the file
	 * 
	 * @param text
	 */
	protected void hashFile() {

		if (waitForUpdate) {
			return;
		}
		
		hashingText = false;
		hashOutput.clear();		
		
		if (filesToHash == null || filesToHash.size() == 0) {
			Dialogs.create().title("Exception").message("An attempt was made to try and hash a " + 
					"null file").showError();
			log.severe("Null file when trying to hash");
			return;
		}

		StringBuilder output = new StringBuilder();
		List<OutputModel> results = new ArrayList<>();
		
		if (filesToHash.size() == 1) {
			
			showObjectMode("Hashing " + filesToHash.get(0).getName(), filesToHash.get(0).length() + " bytes", 
					filesToHash.get(0).getAbsolutePath());	
		} else {
			showObjectMode("Hashing multiple files (" + String.valueOf(filesToHash.size()) + ")", 
					"Multiple sizes", "Maultiple paths");
		}
		
		
		long hashCount = model.hashes.entrySet().parallelStream().filter((e) -> {
			return e.getValue().get();
		}).count();
		long max = filesToHash.size() * hashCount;
		
		
		Dialogs dialogs = Dialogs.create();
		Service<Void> task = new Service<Void>() {
            @Override
            protected Task<Void> createTask() {
                return new Task<Void>() {
        		    @Override         		  
        		    public Void call() {
        		        
        		    	AtomicInteger i = new AtomicInteger(0);
        		    	
        				// Hash each file
        				filesToHash.parallelStream().forEach((File file) -> {
        							
        					FileInputStream stream;
        					try {
        						stream = new FileInputStream(file);
        						byte[] bytes = IOUtils.toByteArray(stream);
        						stream.close();
        				
        						model.hashes.entrySet().parallelStream().filter((e) -> {return e.getValue().get();})
        							.forEach((entry) -> {
        								try {
        									Hasher h = HashFunctionFactory.getHasher(entry.getKey());
        									String hash = h.hash(bytes);
        									output.append(file.getName() + "," + entry.getKey().toString() + "," 
        											+ hash 	+ "\n");
        								
        									results.add(new OutputModel(file.getName(), hash, entry.getKey()));		
        									updateProgress(i.addAndGet(1), max);
        								} catch (HasherException he) {
        									log.severe("HasherException trying to hash file: " + he.getMessage());
        								}
        							});
        					} catch (IOException io) {
        						log.severe("Error hashing a file.");
        					}
        						
        				});
        				Platform.runLater(() -> {
        					showOutput(results, true);	
        					hashOutput.setText(output.toString());
        				});
        		        return null;
        		    }
        		};
            }
        };
		
        // shotcut for zero so we dont get the dialog flashing up then going away again
        if (hashCount > 0) {        
	        dialogs
	        	.title("Hashing")
	        	.message("Hashing " + String.valueOf(filesToHash.size()) + " files using " + 
	        			hashCount + " algorithms.")
	        	.owner(stage.getScene().getWindow())
	        	.showWorkerProgress(task);
        }
        task.start();

	}
	
	/**
	 * Update the hashes of the text
	 * 
	 * @param text
	 */
	protected void hashText(String text) {
		hashOutput.clear();	
		hashingText = true;
		showTextMode();
		if (text == null || text.isEmpty()) {
			return;
		}

		StringBuilder output = new StringBuilder();
		List<OutputModel> results = new ArrayList<>();
		
		// Go through each hash function we have, and if it is enabled hash it
		model.hashes.entrySet().parallelStream().filter((e) -> {return e.getValue().get();})
			.forEach((entry) -> {
				try {
					Hasher h = HashFunctionFactory.getHasher(entry.getKey());
					String hash = h.hash(text);
					output.append(entry.getKey().toString() + "," + hash + "\n");
				
					results.add(new OutputModel(hash, entry.getKey()));
					
					
				} catch (HasherException he) {
					log.severe("HasherException trying to hash text: " + he.getMessage());
				}
			});
		
		showOutput(results, false);
		hashOutput.setText(output.toString());
	}

	
	@FXML protected void handleDebugAction(ActionEvent event) {
		
		Dialogs dialogs = Dialogs.create();
        
		Service<Void> task = new Service<Void>() {
            @Override
            protected Task<Void> createTask() {
                return new Task<Void>() {
        		    @Override public Void call() {
        		        final int max = 2000000;
        		        for (int i=1; i<=max; i++) {
        		            if (isCancelled()) {
        		               break;
        		            }
        		            updateProgress(i, max);
        		        }
        		        return null;
        		    }
        		};
            }
        };
        dialogs.title("Updating List").masthead("Updating the table")
                .message("The table is being updated, please be patient.")
                .owner(stage.getScene().getWindow()).showWorkerProgress(task);
        task.start();

	}
	
	/**
	 * Show the output in the table.
	 */
	protected void showOutput(List<OutputModel> hashes, Boolean files) {
		
		ObservableList<OutputModel> data = FXCollections.observableArrayList();		
		fileColumn.setVisible(files);
		if (hashes != null) {
			hashes.stream().forEach((OutputModel hash) -> {
				data.add(hash);		
			});	
		}
						
		fileColumn.setCellValueFactory(new PropertyValueFactory<OutputModel, String>("filename"));
		hashColumn.setCellValueFactory(new PropertyValueFactory<OutputModel, String>("hash"));
		hashColumn.setCellFactory(TextFieldTableCell.<OutputModel>forTableColumn());
		
		functionColumn.setCellValueFactory(
				new PropertyValueFactory<OutputModel, String>("hashFunction"));
		
		hashTable.setItems(data);
		hashTable.setEditable(true);
		
	}
	
	private void showTextMode() {
		textInput.setVisible(true);
		fileInput.setVisible(false);
	}

	private void showObjectMode(String name, String filesize, String fullpath) {
		textInput.setVisible(false);
		fileInput.setVisible(true);
		filename.setText(name);
		size.setText(filesize);
		path.setText(fullpath);
	}

	/**
	 * Reset everything for a new hash
	 */
	private void reset() {
		text.clear();
		showOutput(null,false);
		hashOutput.clear();
		showTextMode();
		filename.setText("...");
		size.setText("...");
		path.setText("...");
	}
	
	/**
	 * Reset menu
	 * 
	 * @param event
	 */
	@FXML protected void handleOpenMenuAction(ActionEvent event) {
		FileChooser chooser = new FileChooser();
		chooser.setTitle("Select file to hash");
		List<File> files = chooser.showOpenMultipleDialog(stage);
		if (files != null) {			
			filesToHash = files;
			hashFile();
		}
		
	}
	
	/**
	 * Reset menu
	 * 
	 * @param event
	 */
	@FXML protected void handleResetMenuAction(ActionEvent event) {
		reset();
	}
	
	/**
	 * Close menu
	 * 
	 * @param event
	 */
	@FXML protected void handleCloseMenuAction(ActionEvent event) {
		System.exit(0);
	}
	
	/**
	 * About menu
	 * 
	 * @param event
	 */
	@FXML protected void handleAboutMenuAction(ActionEvent event) {
		
	}
	
	/**
	 * Select All menu
	 * 
	 * @param event
	 */
	@FXML protected void handleSelectAllAction(ActionEvent event) {
		waitForUpdate = true;
		for (CheckMenuItem item : allMenu) {
			item.selectedProperty().set(true);
		}
		waitForUpdate = false;
		rehash();
	}
	
	/**
	 * Select None menu
	 * 
	 * @param event
	 */
	@FXML protected void handleSelectNoneAction(ActionEvent event) {
		waitForUpdate = true;
		for (CheckMenuItem item : allMenu) {
			item.selectedProperty().set(false);
		}
		waitForUpdate = false;
		rehash();
	}
	

}





