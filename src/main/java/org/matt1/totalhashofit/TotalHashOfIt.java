package org.matt1.totalhashofit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.security.Security;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.matt1.totalhashofit.hash.HashFunctionFactory;
import org.matt1.totalhashofit.hash.Hasher;
import org.matt1.totalhashofit.hash.HasherException;

/**
 * Main class - will either do command line processing or later on start GUI
 * 
 * @author matt
 *
 */
public class TotalHashOfIt {
	
	/** Default output stream to use for output */
	PrintStream output = System.out;
	
	/** Default logger */
	private static Logger log  = Logger.getLogger(TotalHashOfIt.class.getName());

	/**
	 * Contains all of the command line options for use with args4j
	 * @author matt
	 *
	 */
	private class CommandOptions {
		
		@Option(name="-h", usage="Hash function to use")
		private String hashFunction;

		@Option(name="-f", usage="File mode")
		private Boolean fileMode = false;
		
		@Argument
		private List<String> arguments = new ArrayList<String>();

		public String getHashFunction() {
			return hashFunction;
		}

		public Boolean isFileMode() {
			return fileMode;
		}
		
		public List<String> getArguments() {
			return arguments;
		}
				
	}
	
	/**
	 * Create a new isntance of TotalHashOfIt
	 */
	public TotalHashOfIt() {
		// Register BouncyCastle provider
		Security.addProvider(new BouncyCastleProvider());
	}
	
	/**
	 * Creates a new instance and initialises the application
	 * 
	 * @param arguments
	 */
	public static void main(String[] arguments) {
		TotalHashOfIt totalHashOfIt = new TotalHashOfIt();
		totalHashOfIt.init(arguments);
	}
	
	/**
	 * Initialise the application - start by processing the command line arguments
	 * 
	 * @param arguments
	 */
	public void init(String[] arguments) {
		
		// try parsing the command line to see if we got anything
		CommandOptions commandOptions = new CommandOptions();
		CmdLineParser commandParser = new CmdLineParser(commandOptions);
		
		try {
			commandParser.parseArgument(arguments);							
				
			// Check if we're in commandline mode or not
			if (null != commandOptions.getHashFunction()) {
				noGui(commandOptions);				
			} else {
				gui(arguments);
			}
			
		} catch (CmdLineException cle) {
			log.info("Bad arguments!  Leave blank to start GUI, or:");
			commandParser.printUsage(System.err);
		}
	}
	
	/**
	 * Kick off the GUI
	 */
	private void gui(String[] arguments) {
		TotalHashOfItApplication.launch(TotalHashOfItApplication.class, arguments);
	}
	
	/**
	 * Process the input in command line mode
	 * 
	 * @param commandOptions
	 */
	private void noGui(CommandOptions commandOptions) {
		try {
			// Command line/batch implementation		
			List<String> args = commandOptions.getArguments();
			Hasher h = HashFunctionFactory.getHasher(commandOptions.getHashFunction());
			
			if (commandOptions.isFileMode()) {
				// File mode, treat each string as a file and hash
				File input;
				FileInputStream fis;
				for (String arg : args) {
					input = new File(arg);				
					fis = new FileInputStream(input);
					output.println(h.hash(fis));					
				}
				
			} else {
				// Simple string mode - process each string
				for (String arg : args) {
					output.println(h.hash(arg));
				}
			}
		} catch (HasherException e) {
			log.info("Hashing exception: " + e.getMessage());
		} catch (FileNotFoundException e) {
			log.severe("File not found or cannot access: " + e.getMessage());
		}
	}
	
	/**
	 * Set an output stream for use by the default output
	 * 
	 * @param out
	 */
	public void setOutputStream(PrintStream out) {
		output = out;
	}
}
