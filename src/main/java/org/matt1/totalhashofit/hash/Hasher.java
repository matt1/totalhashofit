package org.matt1.totalhashofit.hash;

import java.io.InputStream;

/**
 * Classes implementing the Hasher interface can provide hashes
 * 
 * @author matt
 *
 */
public interface Hasher {

	/**
	 * Hash a simple string and return the hex hash code
	 * 
	 * @param string
	 * @return
	 * @throws HasherException
	 */
	String hash(String string) throws HasherException;
	
	/**
	 * Hash an input stream (file) and return the hex hash code
	 * 
	 * @param stream
	 * @return
	 * @throws HasherException
	 */
	String hash(InputStream stream) throws HasherException;

	/**
	 * Hash a byte array and return the hex hash code
	 * 
	 * @param string
	 * @return
	 * @throws HasherException
	 */
	String hash(byte[] bytes) throws HasherException;
	
}
