package org.matt1.totalhashofit.hash.functions;

/**
 * Identifies individual hash functions in various places
 * 
 * @author matt
 *
 */
public enum HashFunction {

	MD2("MD2"),
	MD4("MD4"),
	MD5("MD5"),
	SHA1("SHA1"),
	SHA224("SHA224"),
	SHA256("SHA256"),
	SHA384("SHA384"),
	SHA512("SHA512"),
	RIPE128("RIPEMD128"),
	RIPE160("RIPEMD160"),
	RIPE256("RIPEMD256"),
	RIPE320("RIPEMD320"),
	TIGER192_3("TIGER192,3"),
	GOST("GOST3411"),
	WHIRLPOOL("WHIRLPOOL");	
	
	private String name;
	
	HashFunction(String name) {
		this.name = name;
	}
	
	/**
	 * Get the function identifier for the hash function
	 */
	public String toString() {
		return this.name;
	}
}
