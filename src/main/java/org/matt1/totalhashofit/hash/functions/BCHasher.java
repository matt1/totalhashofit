package org.matt1.totalhashofit.hash.functions;

import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import org.apache.commons.io.IOUtils;
import org.bouncycastle.util.encoders.Hex;
import org.matt1.totalhashofit.hash.Hasher;
import org.matt1.totalhashofit.hash.HasherException;

/**
 * Provides hashes for multiple types using BouncyCastle code
 * @author matt
 *
 */
public class BCHasher implements Hasher {

	/** Provider name for Bouncy Castle" */
	private static final String PROVIDER = "BC";
	
	private String function = null;
	
	public BCHasher(String function) {
		this.function = function.toUpperCase();
	}
	
	@Override
	public String hash(byte[] bytes) throws HasherException  {
		String hex = null;
		try {
			byte[] d = MessageDigest.getInstance(function, PROVIDER).digest(bytes);
			hex = Hex.toHexString(d);
		} catch (NoSuchProviderException nspe) {
			throw new HasherException("No such provider " + PROVIDER, nspe);
		} catch (NoSuchAlgorithmException nsae) {
			throw new HasherException("No such algorithm '" + function + "'", nsae);
		}
		return hex;
	}
	
	@Override
	public String hash(String string) throws HasherException {
		return hash(string.getBytes());

	}

	@Override
	public String hash(InputStream stream) throws HasherException {
		try {
			return hash(IOUtils.toByteArray(stream));
		} catch (IOException ioe) {
			throw new HasherException("IO Exception trying to hash file.", ioe);
		}
			
	}

}
