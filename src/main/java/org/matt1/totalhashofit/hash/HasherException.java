package org.matt1.totalhashofit.hash;

/**
 * There was a problem from the hasher
 * 
 * @author matt
 *
 */
public class HasherException extends Exception {

	private static final long serialVersionUID = 3745412773743008001L;

	public HasherException() {
        super();
    }

    public HasherException(String message) {
        super(message);
    }

    public HasherException(String message, Throwable cause) {
        super(message, cause);
    }
	
	
}
