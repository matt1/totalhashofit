package org.matt1.totalhashofit.hash;

import java.util.HashMap;

import org.matt1.totalhashofit.hash.functions.BCHasher;
import org.matt1.totalhashofit.hash.functions.HashFunction;

/**
 * Get an appropriate hasher
 * 
 * @author matt
 *
 */
public class HashFunctionFactory {

	/** List of hashers */
	private static HashMap<String, Hasher> hashers = new HashMap<String, Hasher>();
	static {
		hashers.put(HashFunction.MD5.toString(), new BCHasher("md5"));
		hashers.put(HashFunction.MD4.toString(), new BCHasher("md4"));
		hashers.put(HashFunction.MD2.toString(), new BCHasher("md2"));
		hashers.put(HashFunction.SHA1.toString(), new BCHasher("sha1"));
		hashers.put(HashFunction.SHA224.toString(), new BCHasher("sha-224"));
		hashers.put(HashFunction.SHA256.toString(), new BCHasher("sha-256"));
		hashers.put(HashFunction.SHA384.toString(), new BCHasher("sha-384"));
		hashers.put(HashFunction.SHA512.toString(), new BCHasher("sha-512"));
		hashers.put(HashFunction.RIPE128.toString(), new BCHasher("ripemd128"));
		hashers.put(HashFunction.RIPE160.toString(), new BCHasher("ripemd160"));
		hashers.put(HashFunction.RIPE256.toString(), new BCHasher("ripemd256"));
		hashers.put(HashFunction.RIPE320.toString(), new BCHasher("ripemd320"));
		hashers.put(HashFunction.WHIRLPOOL.toString(), new BCHasher("whirlpool"));
		hashers.put(HashFunction.GOST.toString(), new BCHasher("gost3411"));
		hashers.put(HashFunction.TIGER192_3.toString(), new BCHasher("tiger"));
		
	}
	
	/**
	 * Get Hash function by the hashfunction enum value
	 * 
	 * @param function
	 * @return
	 * @throws HasherException
	 */
	public static Hasher getHasher(HashFunction function) throws HasherException {
		return getHasher(function.toString());
	}
	
	/**
	 * Get a hasher for the given function
	 * @param function
	 * @throws HasherException if the function is not supported.
	 * @return
	 */
	public static Hasher getHasher(String function) throws HasherException {
		// Normalise to upper case.
		String f = function.toUpperCase();
		
		Hasher instance = null;
		
		if (hashers.containsKey(f)) {
			instance = hashers.get(f);
		} else {
			throw new HasherException("Function " + f + " is not supported/implemented.");
		}
		
		return instance;
		
	}
	
}
