package org.matt1.totalhashofit;

import java.io.IOException;
import java.util.logging.Logger;

import org.matt1.totalhashofit.gui.TotalHashOfItGui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


/**
 * JavaFX Application for the GUI app.  
 * 
 * @author matt
 *
 */
public class TotalHashOfItApplication extends Application {
	
	/** Default logger */
	private static Logger log  = Logger.getLogger(TotalHashOfItApplication.class.getName());
	
	@Override
	public void start(Stage stage) throws IOException {
		log.info("Starting Application...");
		
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("./gui/TotalHashOfItGui.fxml"));
        Parent root = fxmlLoader.load();
        TotalHashOfItGui controller = (TotalHashOfItGui) fxmlLoader.getController();
        fxmlLoader.setRoot(root);
        
		
        Scene scene = new Scene(root);		     
        stage.setTitle("Total Hash Of It");
        stage.setScene(scene);
        stage.show();
        
        controller.setStage(stage);
	}

	public static void main(String[] args) {
		launch(args);
	}

}





