package org.matt1.totalhashofit.hash;

import static org.junit.Assert.*;


import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;

/**
 * Test the basic hashing algorithms
 * 
 * @author matt
 *
 */
public class TestHashes {

	InputStream fis;
	
	@Before
	public void setup() {
		
		fis = getClass().getResourceAsStream("testfile.txt");
	}
	
	@Test
	public void unknown() {
		try {
			Hasher h = HashFunctionFactory.getHasher("mdfive");
		} catch (Exception e) {
			assertTrue(e instanceof HasherException);
			assertEquals("Function MDFIVE is not supported/implemented.", e.getMessage());
		}
		
	}
	
	
	@Test
	public void md2() throws HasherException {
		Hasher h = HashFunctionFactory.getHasher("md2");
		assertNotNull(h);
		
		assertEquals("d9cce882ee690a5c1ce70beff3a78c77", h.hash("hello world"));
	}

	@Test
	public void md4() throws HasherException {
		Hasher h = HashFunctionFactory.getHasher("md4");
		assertNotNull(h);
		
		assertEquals("aa010fbc1d14c795d86ef98c95479d17", h.hash("hello world"));
	}
	
	@Test
	public void md5() throws HasherException {
		Hasher h = HashFunctionFactory.getHasher("md5");
		assertNotNull(h);
		
		assertEquals("5eb63bbbe01eeed093cb22bb8f5acdc3", h.hash("hello world"));
		
		assertEquals("ea347f6eabcb6818e8e88487817b7a48", h.hash(fis));				
	}
	
	@Test
	public void sha1() throws HasherException {
		Hasher h = HashFunctionFactory.getHasher("sha1");
		assertNotNull(h);
		
		assertEquals("2aae6c35c94fcfb415dbe95f408b9ce91ee846ed", h.hash("hello world"));
	}

	@Test
	public void sha224() throws HasherException {
		Hasher h = HashFunctionFactory.getHasher("sha224");
		assertNotNull(h);
		
		assertEquals("2f05477fc24bb4faefd86517156dafdecec45b8ad3cf2522a563582b", h.hash("hello world"));
	}


	@Test
	public void ripe128() throws HasherException {
		Hasher h = HashFunctionFactory.getHasher("ripemd128");
		assertNotNull(h);
		
		assertEquals("c52ac4d06245286b33953957be6c6f81", h.hash("hello world"));
	}
	
	@Test
	public void ripe160() throws HasherException {
		Hasher h = HashFunctionFactory.getHasher("ripemd160");
		assertNotNull(h);
		
		assertEquals("98c615784ccb5fe5936fbc0cbe9dfdb408d92f0f", h.hash("hello world"));
	}
	
	@Test
	public void ripe256() throws HasherException {
		Hasher h = HashFunctionFactory.getHasher("ripemd256");
		assertNotNull(h);
		
		assertEquals("0d375cf9d9ee95a3bb15f757c81e93bb0ad963edf69dc4d12264031814608e37", h.hash("hello world"));
	}
	
	@Test
	public void ripe320() throws HasherException {
		Hasher h = HashFunctionFactory.getHasher("ripemd320");
		assertNotNull(h);
		
		assertEquals("0e12fe7d075f8e319e07c106917eddb0135e9a10aefb50a8a07ccb0582ff1fa27b95ed5af57fd5c6", h.hash("hello world"));
	}
	
	@Test
	public void sha256() throws HasherException {
		Hasher h = HashFunctionFactory.getHasher("sha256");
		assertNotNull(h);
		
		assertEquals("b94d27b9934d3e08a52e52d7da7dabfac484efe37a5380ee9088f7ace2efcde9", h.hash("hello world"));
	}
	
	@Test
	public void sha384() throws HasherException {
		Hasher h = HashFunctionFactory.getHasher("sha384");
		assertNotNull(h);
		
		assertEquals("fdbd8e75a67f29f701a4e040385e2e23986303ea10239211af907fcbb83578b3e417cb71ce646efd0819dd8c088de1bd", h.hash("hello world"));
	}
	
	@Test
	public void sha512() throws HasherException {
		Hasher h = HashFunctionFactory.getHasher("sha512");
		assertNotNull(h);
		
		assertEquals("309ecc489c12d6eb4cc40f50c902f2b4d0ed77ee511a7c7a9bcd3ca86d4cd86f989dd35bc5ff499670da34255b45b0cfd830e81f605dcf7dc5542e93ae9cd76f", h.hash("hello world"));
	}
	
	@Test
	public void whirlpool() throws HasherException {
		Hasher h = HashFunctionFactory.getHasher("whirlpool");
		assertNotNull(h);
		
		assertEquals("8d8309ca6af848095bcabaf9a53b1b6ce7f594c1434fd6e5177e7e5c20e76cd30936d8606e7f36acbef8978fea008e6400a975d51abe6ba4923178c7cf90c802", h.hash("hello world"));
	}
	
	@Test
	public void gost() throws HasherException {
		Hasher h = HashFunctionFactory.getHasher("gost3411");
		assertNotNull(h);
		
		assertEquals("c5aa1455afe9f0c440eec3c96ccccb5c8495097572cc0f625278bd0da5ea5e07", h.hash("hello world"));
	}
	
	@Test
	public void tiger192() throws HasherException {
		Hasher h = HashFunctionFactory.getHasher("tiger192,3");
		assertNotNull(h);
		
		assertEquals("4c8fbddae0b6f25832af45e7c62811bb64ec3e43691e9cc3", h.hash("hello world"));
	}
	
}
