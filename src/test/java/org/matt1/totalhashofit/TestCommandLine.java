package org.matt1.totalhashofit;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Tests running the app from the command line
 * 
 * @author matt
 *
 */
public class TestCommandLine {

	PrintStream out;
	ByteArrayOutputStream bytes;
	TotalHashOfIt totalHashOfit;
	
	@Before
	public void setup() {
		bytes = new ByteArrayOutputStream();
		out = new PrintStream(bytes);
		totalHashOfit = new TotalHashOfIt();
		totalHashOfit.setOutputStream(out);
	}
	
	@After
	public void cleanup() throws IOException {
		out.close();
	}
	
	private String out() {
		return new String(bytes.toByteArray()).trim();
	}
	
	@Test
	public void testSingleStringHash() throws IOException {
		String[] args = {"-h", "md5" ,"hello world"};
		totalHashOfit.init(args);		
		assertEquals("5eb63bbbe01eeed093cb22bb8f5acdc3", out());
		
	}
	
	@Test
	public void testMultipleStringHash() throws IOException {
		String[] args = {"-h", "md5", "one", "two", "three"};
		totalHashOfit.init(args);

		assertEquals("f97c5d29941bfb1b2fdab0874906ab82\nb8a9f715dbb64fd5c56e7783c6820a61\n" + 
				"35d6d33467aae9a2e3dccb4b6b027878", out());
		
	}
		
}
